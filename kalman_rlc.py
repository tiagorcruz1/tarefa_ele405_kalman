# -*- coding: utf-8 -*-
"""
Created on Sat May 11 15:31:07 2019

@author: tiago
"""

import numpy as np
import matplotlib.pyplot as plt
from math import sqrt
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset # efeito zoom (caixa)

plt.close('all')


# tempo de amostragem
h = 0.001

# numero de amostras
N = int(3/h)

# vetor de tempo
t = np.arange(0, 3, h)

# componentes
Z_1 = Z_2 = 3
C = 3
L = 2

# matrizes espaco de estados
A = np.array([[-h/(Z_1*C), 1-h/C], [-h/L*(1+4*Z_2), 1-Z_2*h/L]])
B = np.array([[h/(Z_1*C)], [0]])
C = np.array([[4, 1]])
D = 0

# declaracao de variaveis
x  = np.zeros((N,2)) # estados
y  = np.zeros((N,1)) # saida
xp = np.zeros((N,2)) # valor de predicao
xe = np.zeros((N,2)) # valor estimado
pp = np.zeros((2,2)) # incerteza da predicao
pe = np.zeros((2,2)) # incerteza de valor estimado
S  = np.zeros((N,1)) # auxiliar
kk = np.zeros((N,2)) # ganho do filtro

# condicoes iniciais
x0 = np.array([[0.0001, 0]])
p0 = np.array([[0.002, 0],[0,0.01]])# incerteza inicial
x[0]  = x0
pe[0] = p0[0]
pe[1] = p0[1]

Q = np.array([[1e-12,0],[0,1e-12]]) # variancia dos elementos externos (desconhecido)
R = np.array([[1e-9]]) # variancia do ruido
vk = sqrt(1e-9)*np.random.randn(N).T

# entrada
u = 5*np.ones((N,1))
#u[0] = 1
#for i in range(0,N):
#    u[i] = np.array(np.sin(np.pi*i/10000000))

# equações de estado 
for k in range(0,N-1):
    x[k+1] = A@x[k] + B@u[k]

# equacaode saida
for k in range(0, N):
    y[k] = C@x[k] + vk[k]

# equacoes do filtro de Kalman
for k in range(0, N-1):
    xp[k] = A@xe[k] + B@u[k]
    pp = A@pe@A.T + Q
    S[k] = C@pp@C.T+R
    kk[k] = (pp@C.T/S[k]).reshape(1,2)
    pe = (np.identity(2)-kk[k].reshape(2,1)@C)@pp # trata kk como coluna
    xe[k+1] = xp[k] + kk[k]*(y[k]-C@xp[k])

# plots
plt.figure(1)
plt.subplot(2,1,1)
#plt.plot(t,u)
#plt.title('Entrada'), plt.xlabel('Tempo [s]'), plt.ylabel('Tensão [V]')
plt.plot(t,y, '--', color='purple', linewidth=0.5)
plt.plot(t,x[:,0], color = 'blue', label = 'v1_real')
plt.plot(t,xe[:,0], color = 'green', label = 'v1_estimado')
plt.legend()
plt.subplot(2,1,2)
plt.plot(t,x[:,1], color = 'cyan', label = 'i3_real')
plt.plot(t,xe[:,1], color = 'black', label = 'i3_estimado')
plt.legend()

size=30
#plt.rc('font', size=14)          # controls default text sizes
plt.rc('axes', titlesize=size)     # fontsize of the axes title
plt.rc('axes', labelsize=size)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=size)    # fontsize of the tick labels
plt.rc('ytick', labelsize=size)    # fontsize of the tick labels
plt.rc('legend', fontsize=size)    # legend fontsize
#plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

# plot com zoom
fig, ax = plt.subplots() # create a new figure with a default 111 subplot
ax.plot(t,y, '--', color='purple', linewidth=0.5)
ax.plot(t,x[:,0], '-.', color = 'blue', label = 'v1_real')
ax.plot(t,xe[:,0], color = 'green', label = 'v1_estimado')
ax.set_xlabel("Tempo [s]")
ax.set_title("Filtro de Kalman")
ax.legend(('Medição (i4)', 'v1 real', 'v1 estimado'), loc='upper right')
axins = zoomed_inset_axes(ax, 15, loc='right') # zoom-factor: 2.5, location: upper-left
axins.plot(t,x[:,0], '-.', color = 'blue')
axins.plot(t,xe[:,0], color = 'green')
x1, x2, y1, y2 = 1, 1.1, 0.00009, 0.00012 # specify the limits
axins.set_xlim(x1, x2) # apply the x-limits
axins.set_ylim(y1, y2) # apply the y-limits
plt.yticks(visible=False)
plt.xticks(visible=False)
mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.5")

fig, ax = plt.subplots() # create a new figure with a default 111 subplot
ax.plot(t,x[:,1], '-.', color = 'blue', label = 'i3_real')
ax.plot(t,xe[:,1], color = 'green', label = 'i3_estimado')
ax.set_xlabel("Tempo [s]")
ax.set_title("Filtro de Kalman")
ax.legend(('i3 real', 'i3 estimado'), loc='upper right')
axins = zoomed_inset_axes(ax, 5, loc='right') # zoom-factor: 2.5, location: upper-left
axins.plot(t,x[:,1], '-.', color = 'blue')
axins.plot(t,xe[:,1], color = 'green')
x1, x2, y1, y2 = 0.7, 1, -0.00046, -0.00044 # specify the limits
axins.set_xlim(x1, x2) # apply the x-limits
axins.set_ylim(y1, y2) # apply the y-limits
plt.yticks(visible=False)
plt.xticks(visible=False)
mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.5")